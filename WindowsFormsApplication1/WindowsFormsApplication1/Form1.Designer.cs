﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.JuodaKava = new System.Windows.Forms.Button();
            this.BaltaKava = new System.Windows.Forms.Button();
            this.LateKava = new System.Windows.Forms.Button();
            this.MochaKava = new System.Windows.Forms.Button();
            this.Capuchino = new System.Windows.Forms.Button();
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.papild = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // JuodaKava
            // 
            this.JuodaKava.Location = new System.Drawing.Point(12, 12);
            this.JuodaKava.Name = "JuodaKava";
            this.JuodaKava.Size = new System.Drawing.Size(75, 43);
            this.JuodaKava.TabIndex = 0;
            this.JuodaKava.Text = "Juoda";
            this.JuodaKava.UseVisualStyleBackColor = true;
            this.JuodaKava.Click += new System.EventHandler(this.JuodaKava_Click);
            // 
            // BaltaKava
            // 
            this.BaltaKava.Location = new System.Drawing.Point(93, 14);
            this.BaltaKava.Name = "BaltaKava";
            this.BaltaKava.Size = new System.Drawing.Size(75, 43);
            this.BaltaKava.TabIndex = 1;
            this.BaltaKava.Text = "Balta";
            this.BaltaKava.UseVisualStyleBackColor = true;
            this.BaltaKava.Click += new System.EventHandler(this.BaltaKava_Click);
            // 
            // LateKava
            // 
            this.LateKava.Location = new System.Drawing.Point(12, 63);
            this.LateKava.Name = "LateKava";
            this.LateKava.Size = new System.Drawing.Size(75, 43);
            this.LateKava.TabIndex = 2;
            this.LateKava.Text = "Late";
            this.LateKava.UseVisualStyleBackColor = true;
            this.LateKava.Click += new System.EventHandler(this.LateKava_Click);
            // 
            // MochaKava
            // 
            this.MochaKava.Location = new System.Drawing.Point(12, 112);
            this.MochaKava.Name = "MochaKava";
            this.MochaKava.Size = new System.Drawing.Size(75, 43);
            this.MochaKava.TabIndex = 3;
            this.MochaKava.Text = "Mocha";
            this.MochaKava.UseVisualStyleBackColor = true;
            this.MochaKava.Click += new System.EventHandler(this.MochaKava_Click);
            // 
            // Capuchino
            // 
            this.Capuchino.Location = new System.Drawing.Point(93, 63);
            this.Capuchino.Name = "Capuchino";
            this.Capuchino.Size = new System.Drawing.Size(75, 43);
            this.Capuchino.TabIndex = 4;
            this.Capuchino.Text = "Capuchino";
            this.Capuchino.UseVisualStyleBackColor = true;
            this.Capuchino.Click += new System.EventHandler(this.Capuchino_Click);
            // 
            // Ekranas
            // 
            this.Ekranas.BackColor = System.Drawing.Color.Black;
            this.Ekranas.ForeColor = System.Drawing.Color.White;
            this.Ekranas.Location = new System.Drawing.Point(12, 161);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(241, 199);
            this.Ekranas.TabIndex = 5;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ekranas.TextChanged += new System.EventHandler(this.Ekranas_TextChanged);
            // 
            // papild
            // 
            this.papild.Location = new System.Drawing.Point(261, 24);
            this.papild.Name = "papild";
            this.papild.Size = new System.Drawing.Size(75, 23);
            this.papild.TabIndex = 6;
            this.papild.Text = "Reset";
            this.papild.UseVisualStyleBackColor = true;
            this.papild.Click += new System.EventHandler(this.papild_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 372);
            this.Controls.Add(this.papild);
            this.Controls.Add(this.Ekranas);
            this.Controls.Add(this.Capuchino);
            this.Controls.Add(this.MochaKava);
            this.Controls.Add(this.LateKava);
            this.Controls.Add(this.BaltaKava);
            this.Controls.Add(this.JuodaKava);
            this.Name = "Form1";
            this.Text = "Kavos Aparatas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button JuodaKava;
        private System.Windows.Forms.Button BaltaKava;
        private System.Windows.Forms.Button LateKava;
        private System.Windows.Forms.Button MochaKava;
        private System.Windows.Forms.Button Capuchino;
        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button papild;
    }
}

