﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
        
    {
        const int maxPuodukuSkaicius = 10;
        int puodukSkaicius = 0;
        string[] logas = new string[maxPuodukuSkaicius];


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Ekranas_TextChanged(object sender, EventArgs e)
        {

        }

        private void JuodaKava_Click(object sender, EventArgs e)
        {
            Atvaizduok("Juoda");
        }

        private void LateKava_Click(object sender, EventArgs e)
        {
            Atvaizduok("Late");
        }

        private void BaltaKava_Click(object sender, EventArgs e)
        {
            Atvaizduok("Balta");
        }

        private void Capuchino_Click(object sender, EventArgs e)
        {
            Atvaizduok("Capuchino");
        }

        private void MochaKava_Click(object sender, EventArgs e)
        {
            Atvaizduok("Mocha");
        }
        private void Atvaizduok(string kava)
        {
            if (puodukSkaicius >= 10)
            {
                Ekranas.Text = "Puoduku nebeliko";
                return;
            }


            for (int i = 0; i < 100; i++)
            {
                Ekranas.Text = i.ToString();
                Thread.Sleep(1);
                Ekranas.Refresh();
            }

            Ekranas.Text = kava + " kava pagaminta";
            puodukSkaicius++;

        }

        public void papild_Click(object sender, EventArgs e)
        {
        
            Form2 f2 = new Form2();
            DialogResult rezult = f2.ShowDialog(this);
            if (rezult == DialogResult.OK)
            {
                if (f2.slaptazodis.Text == "123")
                {
                    Ekranas.Text = "";
                    puodukSkaicius = 0;
                    MessageBox.Show("Reset OK");
                }
                
               
            }
            //f2.ShowDialog();


            //Ekranas.Text = "";
            //puodukSkaicius = 0;
                
           
        }
    }
}
